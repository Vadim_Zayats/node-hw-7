import { Request, Response } from "express";
import { newspostTable } from "../service/crud-db-service";

export const getAllPosts = async (req: Request, res: Response) => {
  try {
    const page = parseInt(req.query.page as string) || 1;
    const size = parseInt(req.query.size as string) || 10;

    const allPosts = await newspostTable.getAll(page, size);
    res.json(allPosts);
  } catch (error) {
    console.error("Помилка при отриманні всіх даних:", error);
    res.status(500).send("Помилка на сервері при отриманні всіх даних");
  }
};

export const getPostById = async (req: Request, res: Response) => {
  try {
    const allPosts = await newspostTable.getById(parseInt(req.params.id));
    if (!allPosts) {
      return res.status(404).send("Пост не знайдено");
    }
    res.json(allPosts);
  } catch (error) {
    console.error("Помилка при отриманні посту по id:", error);
    res.status(500).send("Помилка на сервері при отриманні посту по id");
  }
};

export const createNewPost = async (req: Request, res: Response) => {
  try {
    const allPosts = await newspostTable.create(req.body);
    res.json(allPosts);
  } catch (error) {
    console.error("Помилка при створенні нового посту:", error);
    res.status(500).send("Помилка на сервері");
  }
};

export const editPost = async (req: Request, res: Response) => {
  try {
    const allPosts = await newspostTable.update(
      parseInt(req.params.id),
      req.body
    );
    if (!allPosts) {
      return res.status(404).send("Пост не знайдено");
    }
    res.json(allPosts);
  } catch (error) {
    console.error("Помилка при редагуванні посту:", error);
    res.status(500).send("Помилка на сервері при редагуванні посту");
  }
};
