export interface Data {
  id: number;
  title: string;
  text: string;
  createDate: Date;
}

export interface Schema {
  [key: string]: any;
}

export interface InputData {
  title: string;
  text: string;
}

export interface GetTableOperations {
  getAll: (
    page: number,
    size: number
  ) => Promise<{ paginatedData: Data[]; totalLength: number } | undefined>;
  getById: (id: number) => Promise<Data | undefined>;
  create: (dataArray: InputData) => Promise<Data>;
  update: (
    id: number,
    newData: Partial<Data>
  ) => Promise<Data | string | undefined>;
  delete: (id: number) => Promise<number | undefined | null>;
}
