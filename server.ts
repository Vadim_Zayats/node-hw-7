import express, { Request, Response } from "express";
import path from "path";
import newspostsConfigs from "./routes/newspost";
import morgan from "morgan";

const app = express();

const HOST = process.env.HOST || "localhost";
const PORT = process.env.PORT || 8000;

app.use(morgan("dev"));

app.use(express.static(path.join(__dirname, "client/dist")));

app.get("/", (req: Request, res: Response) => {
  // Відправлення статичного файлу index.html
  res.sendFile(path.join(__dirname, "client", "dist", "index.html"), (err) => {
    if (err) {
      // Обробка помилки, якщо не вдалося надіслати файл
      console.error("Error sending index.html:", err);
      res.status(500).send("Internal Server Error");
    }
  });
});

app.use("/api/newsposts/", newspostsConfigs);
app.use("/api/newsposts/:id", newspostsConfigs);

app.listen(PORT, () => {
  console.log(`Server is running on http://${HOST}:${PORT}`);
});
