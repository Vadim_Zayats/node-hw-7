import styles from "./card.module.scss";
import { Link } from "react-router-dom";
import { Data } from "../../../../../types/types";

export function Card({ post }: { post: Data }) {
  return (
    <Link className={`${styles["card__link"]}`} to={`/post/${post.id}`}>
      <li className={`${styles["card__item"]}`}>
        <h3>{post.title}</h3>
        <p className={`${styles["card__text"]}`}>{post.text}</p>
      </li>
    </Link>
  );
}
