import { useState, useEffect } from "react";
import styles from "./main.module.scss";
import { Link } from "react-router-dom";
import { Data } from "../../../../../types/types";
import { Card } from "../Card/Card";

export function Main() {
  const [posts, setPosts] = useState<
    { paginatedData: Data[]; totalLength: number } | undefined
  >();
  const [page, setPage] = useState<number>(1);
  const [size] = useState<number>(2);

  async function getPosts(
    page: number,
    size: number
  ): Promise<Data[] | undefined | void> {
    try {
      const res = await fetch(
        `http://localhost:8000/api/newsposts?page=${page}&size=${size}`
      );
      if (!res.ok) {
        throw new Error("Server error");
      }
      const data = await res.json();
      return setPosts(data);
    } catch (error) {
      return console.log(error);
    }
  }

  useEffect(() => {
    getPosts(page, size);
  }, [page, size]);

  const handleNext = () => {
    setPage((prevState) => prevState + 1);
  };
  const handlePrev = () => {
    if (page !== 1) {
      setPage((prevState) => prevState - 1);
    }
  };

  return (
    <>
      <Link to={`/newpost`}>
        <button className={`${styles["posts__add-new"]}`}>Додати новину</button>
      </Link>

      <ul className={`${styles["posts__list"]}`}>
        {posts &&
          posts.paginatedData.map((post: Data) => {
            return <Card post={post} />;
          })}
      </ul>
      {page !== 1 && (
        <button className={`${styles["posts__add-new"]}`} onClick={handlePrev}>
          Назад
        </button>
      )}
      {posts && posts.totalLength > page * size && (
        <button className={`${styles["posts__add-new"]}`} onClick={handleNext}>
          Далі
        </button>
      )}
    </>
  );
}
